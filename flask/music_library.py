import sqlite3
from functools import wraps

from flask import Flask, redirect, render_template, request

app = Flask(__name__)

connected_db = 'DB_music.db'    # указываем какую конкретно БД подключаем


def open_sqlite_db(data_base):
    '''Функция-декоратор подключения БД'''
    def connection_func(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            with sqlite3.connect(data_base) as conn:
                cursor = conn.cursor()
                return func(cursor, conn, *args, **kwargs)
        return wrapper
    return connection_func


@app.route('/my_music/', methods=['GET', 'POST'])
@open_sqlite_db(connected_db)
def my_music(cursor, conn):
    # выводим все треки, которые есть, на данный момент, в БД
    if request.method == 'GET':
        cursor.execute('SELECT * FROM music;')
        data = cursor.fetchall()
    # POST-запрос на главной странице для удаления соответствующего трека
    elif request.method == 'POST' and request.values.get('del'):
        id = request.values.get('del')    # определяем id соответствующей кнопки (соответстующего трека)
        cursor.execute('DELETE FROM music WHERE id = ?', (id, ))
        conn.commit()                     # коммитим изменения в БД
        return redirect('/my_music/')         # возвращаемся на главную страницу с обновленными данными
    return render_template('music_library.html', data=data)


@app.route('/my_music/add_music/', methods=['GET', 'POST'])
@open_sqlite_db(connected_db)
def add_music(cursor, conn):
    # POST для добавления новых записей в БД
    if request.method == 'POST':
        title = request.values.get('input_title')            # считываем введеные данные в input_title
        author = request.values.get('input_author')          # считываем введеные данные в input_author
        duration = request.values.get('input_duration')      # считываем введеные данные в input_duration
        if not title or not author or not duration:          # проверяем все ли поля заполнены
            return render_template('error_info.html')  # отправляю на страницу с ошибкой если не все поля заполнены
        else:
            cursor.execute('INSERT INTO music (title, author, duration) VALUES (?, ?, ?);',
                           (title, author, duration))
            conn.commit()                          # коммитим изменения в БД
            return redirect(f'/my_music/')         # возвращаемся на главную страницу с обновленными данными
    return render_template('add_music.html')


@app.route('/my_music/search_music/', methods=['GET'])
@open_sqlite_db(connected_db)
def search_music(cursor, conn):
    text_input = request.values.get('text_input')
    if text_input:
        if request.values.get('select') == 'title':
            cursor.execute('SELECT * FROM music WHERE title LIKE ?;', (f'%{text_input}%', ))
        else:
            cursor.execute('SELECT * FROM music WHERE author LIKE ?;', (f'%{text_input}%', ))
        conn.commit()                       # коммитим изменения в БД
        data = cursor.fetchall()            # записываем выбранные данные из БД в переменную data
        return render_template('music_library.html', data=data)     # выводим результаты поиска
    return render_template('search_music.html')


@app.route('/my_music/rename_music/', methods=['GET', 'POST'])
@open_sqlite_db(connected_db)
def rename_music(cursor, conn):
    button_id = request.values.get('button_rename')  # считываю id кнопки (id соответсвующего трека)
    # записываем в data_track актуальную информацию о треке
    cursor.execute('SELECT id, title, author FROM music WHERE id=?;', (button_id, ))
    conn.commit()
    data_track = cursor.fetchone()
    # после нажатия кнопки Save, отправляем изменения в БД
    if request.method == 'POST':
        id = request.values.get('rename')
        text_input = request.values.get('text_input')
        if not text_input:                   # если в text_input ничего не ввели в input на странице html
            text_input = '"NO DATA"'         # присваиваем значение "NO DATA"
        # определяем какое поле (title или author) выбрано для внесения изменений
        if request.values.get('select') == 'title':
            cursor.execute('UPDATE music SET title = ? WHERE id = ?', (text_input, str(id)))
        else:
            cursor.execute('UPDATE music SET author = ? WHERE id = ?', (text_input, str(id)))
        conn.commit()                               # коммитим изменения в БД
        return redirect(f'/my_music/')              # возвращаемся на главную страницу с обновленными данными
    return render_template('rename.html', data_track=data_track)


if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=5000)