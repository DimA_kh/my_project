import requests
from flask import Flask, render_template, request
from faker import Faker
import json


app = Flask(__name__)
fake = Faker()


@app.route('/requirements/')
def requirements():
    with open('requirements.txt', encoding='utf-8') as lib_text:
        lib_list = lib_text.readlines()
    return render_template('requirements.html', requirements=lib_list)


@app.route('/generate-users/')
def generate_users():
    res: dict = {}
    amount: str = request.values.get('amount')
    if amount:
        counter: int = 0
        while counter < int(amount):
            counter += 1
            res[fake.name()] = fake.email()
    return render_template('generate_users.html', res=res, amount=amount)


@app.route('/space/')
def space():
    response = requests.get('http://api.open-notify.org/astros.json')
    if response.status_code == 200:
        res_num = json.loads(response.text)['number']
        dict_people = json.loads(response.text)['people']
        return render_template('space.html', dict_people=dict_people, res_num=res_num)


if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=5000)

